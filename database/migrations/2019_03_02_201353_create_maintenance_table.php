<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('where')->nullable()->index();
            $table->string('priority')->default('Normale')->index();
            $table->integer('taken_charge_by')->nullable()->index();
            $table->integer('verified_by')->nullable()->index();

            $table->string('status')->default('Nuovo')->index();

            $table->text('notes')->nullable();
            $table->integer('notes_by')->nullable();
            $table->timestamp('notes_added_at')->nullable();

            $table->timestamp('fixed_at')->nullable();
            $table->timestamp('taken_charge_at')->nullable();

            $table->integer('added_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance');
    }
}
