<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Illuminate\Support\Facades\Route;

	class AdminMaintenanceController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			#echo 'user privilege id '.CRUDBooster::getMyPrivilegeId();

			$cms_users_id = CRUDBooster::myId();
			$user = DB::table('cms_users')->where('id',$cms_users_id)->first();
			$ut = $user->id_cms_privileges;

			$cfg = [

				1 => [
					'button_edit'=>true,
					'button_detail'=>true,
					'status'=>'Neuf;Prendre en Charge;En attente de pièces de rechange;Terminé;Terminé et Vérifié',
					'disabled' => []
				],
				2 => [
					'button_edit'=>true,
					'button_detail'=>false,
					'status'=>'Neuf;Prendre en Charge;En attente de pièces de rechange;Terminé;Terminé et Vérifié',
					'disabled' => []
				],
				3 => [
					'button_edit'=>true,
					'button_detail'=>true,
					'status'=>'Neuf;Prendre en Charge;En attente de pièces de rechange;Terminé',
					'disabled' => []
				],
				4 => [
					'button_edit'=>true,
					'button_detail'=>true,
					'status'=>'Neuf;Prendre en Charge;En attente de pièces de rechange;Terminé;Terminé et Vérifié',
					'disabled' => []
				]

			];

			#echo $ut;

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "title";
			$this->limit = "50";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon_text";
			if($ut==1 || $ut==2 || $ut==3){
				$this->button_add = true;
			}else{
				$this->button_add = false;
			}

			#$this->button_edit = $cfg[$ut]['button_edit'];
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = $cfg[$ut]['button_detail'];
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "maintenance";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			if($ut == 3 || $ut == 4 ){
				$this->col = [];
				$this->col[] = ["label"=>"ID","name"=>"(select maintenance.id) as id"];
				$this->col[] = ["label"=>"Inséré le","name"=>"created_at","callback_php"=>'date("d/m/Y",strtotime($row->created_at))'];
				$this->col[] = ["label"=>"Titre","name"=>"title"];
				$this->col[] = ["label"=>"Priorité","name"=>"priority"];
				$this->col[] = ["label"=>"Où","name"=>"where"];
				$this->col[] = ["label"=>"Status","name"=>"status"];
				$this->col[] = ["label"=>"Problem Type","name"=>"type"];


			}

			if($ut == 2){
				$this->col = [];
				$this->col[] = ["label"=>"ID","name"=>"(select maintenance.id) as id"];
				$this->col[] = ["label"=>"Inséré le","name"=>"created_at","callback_php"=>'date("d/m/Y",strtotime($row->created_at))'];
				$this->col[] = ["label"=>"Titre","name"=>"title"];
				$this->col[] = ["label"=>"Priorité","name"=>"priority"];
				$this->col[] = ["label"=>"Où","name"=>"where"];
				$this->col[] = ["label"=>"Status","name"=>"status"];
				$this->col[] = ["label"=>"Problem Type","name"=>"type"];
				$this->col[] = ["label"=>"En charge a","name"=>"taken_charge_by","join"=>"cms_users,name"];
				$this->col[] = ["label"=>"Vérifié par","name"=>"verified_by","join"=>"cms_users,name"];

			}

			if($ut == 1){
				$this->col = [];
				$this->col[] = ["label"=>"ID","name"=>"(select maintenance.id) as id"];
				$this->col[] = ["label"=>"Inséré le","name"=>"created_at","callback_php"=>'date("d/m/Y",strtotime($row->created_at))'];
				$this->col[] = ["label"=>"Titre","name"=>"title"];
				$this->col[] = ["label"=>"Où","name"=>"where"];
				$this->col[] = ["label"=>"Priorité","name"=>"priority"];
				$this->col[] = ["label"=>"Status","name"=>"status"];
				$this->col[] = ["label"=>"Problem Type","name"=>"type"];
				$this->col[] = ["label"=>"En charge a","name"=>"taken_charge_by","join"=>"cms_users,name"];
				$this->col[] = ["label"=>"Prendre en Charge il","name"=>"taken_charge_at","callback_php"=>'date("d/m/Y",strtotime($row->taken_charge_at))'];
				$this->col[] = ["label"=>"Résolu","name"=>"fixed_at","callback_php"=>'date("d/m/Y",strtotime($row->fixed_at))'];
				$this->col[] = ["label"=>"Time","name"=>"(select time_format(timediff(maintenance.fixed_at,maintenance.taken_charge_at),'%H:%i')) as hours"];
				$this->col[] = ["label"=>"Vérifié par","name"=>"verified_by","join"=>"cms_users,name"];
		  }
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Titre','disabled'=>in_array('title',$cfg[$ut]['disabled']),'name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Description','name'=>'description','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Où','name'=>'where','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Priorité','name'=>'priority','type'=>'select','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'Normale;Urgente;Urgente stanza bloccata;Urgente cliente in camera'];
			if($ut == 1 || $ut > 2){
				$this->form[] = ['label'=>'Status','name'=>'status','type'=>'select','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>$cfg[$ut]['status'],'disabled'=>true];
				$this->form[] = ['label'=>'En charge a','name'=>'taken_charge_by','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
				$this->form[] = ['label'=>'Résolu','name'=>'fixed_at','type'=>'datetime','validation'=>'date_format:Y-m-d H:i:s','width'=>'col-sm-10'];
				$this->form[] = ['label'=>'Vérifié par','name'=>'verified_by','type'=>'select2','validation'=>'integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
				$this->form[] = ['label'=>'Notes','name'=>'notes','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10'];
			}else{
				$this->form[] = ['label'=>'Status','name'=>'status','type'=>'hidden','value'=>'Neuf'];

			}
			$this->form[] = ["label"=>"Problem Type","name"=>"type","type"=>"select","validation"=>"","width"=>"col-sm-10","dataenum"=>"Climatisasion;Hydraulique;Electicité;Mobilier;Nettoyage;Autre"];
		# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Title","name"=>"title","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			//$this->form[] = ["label"=>"Description","name"=>"description","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Where","name"=>"where","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Priority","name"=>"priority","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Taken Charge By","name"=>"taken_charge_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Verified By","name"=>"verified_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Notes","name"=>"notes","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Notes By","name"=>"notes_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Notes Added At","name"=>"notes_added_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
			//$this->form[] = ["label"=>"Fixed At","name"=>"fixed_at","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
					ob_start();
					?>

					$(function(){
						$('#table_dashboard tbody tr').each(function(){
							var tr = $(this);

							var rowStatus = tr.find('td').eq(6).html();

							switch(rowStatus){
								case 'Neuf':
									tr.addClass('danger')
								break;
								case 'Prendre en Charge':
									tr.addClass('warning')
								break;
								case 'En attente de pièces de rechange':
									tr.addClass('info')
								break;
								case 'Terminé':
									tr.addClass('success')
								break;
								case 'Terminé et Vérifié':
									tr.addClass('success')
								break;
							}
						})
					})

					<?php
					$script_js = ob_get_clean();

	        $this->script_js = $script_js;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */

					$this->pre_detail_html = '';


					$post_detail_html = '
					<div class="container-fluid">
						<div class="row">
						  <div class="col-md-12">
								<form class="" action="/admin/maintenance/set-maintenance-notes/[$id]" method="get">
									<div class="form-group">
										<label for="">Note</label>
										<textarea name="notes" class="form-control" rows="8" cols="80"></textarea>
										<p class="help-block">Notes de technicien</p>
									</div>
									<div class="form-group">
									  <button type="submit" class="btn btn-primary btn-lg" name="button">Enregistrer des Notes</button>
									</div>

								</form>
						  </div>
						</div>
					</div>
					<hr>';

					$id = Session::get('current_row_id');

					$maintenance = DB::table('maintenance')->where('id',$id)->first();

					#$statuses = explode(';',$cfg[$ut]['status']);
					$statuses = [];
					if($maintenance->taken_charge_by>0){
						if($maintenance->fixed_at!= NULL){
							if($maintenance->verified_by>0){

							}else{
								if($ut==4){
										$statuses[] = 'Terminé et Vérifié';
								}
							}
						}else{
							$statuses[] = 'En attente de pièces de rechange';
							$statuses[] = 'Terminé';
						}
					}else{
						$statuses[] = 'Prendre en Charge';
					}
					foreach($statuses as $index => $status){



								$post_detail_html .= '<a style="margin:0px 10px 10px 0px" class="btn btn-lg btn-primary" href="/admin/maintenance/set-maintenance-status/[$id]/'.$status.'">'.$status.'</a>';


					}




					$this->post_detail_html = $post_detail_html;

	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }

			public function getDetail($id){
				Session::put('current_row_id', $id);

				$this->cbLoader();

				$row = DB::table($this->table)->where($this->primary_key, $id)->first();

				$this->data['id'] = $id;

				if (! CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
						CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
								'name' => $row->{$this->title_field},
								'module' => CRUDBooster::getCurrentModule()->name,
						]));
						CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
				}

				$module = CRUDBooster::getCurrentModule();

				$page_menu = Route::getCurrentRoute()->getActionName();
				$page_title = trans("crudbooster.detail_data_page_title", ['module' => $module->name, 'name' => $row->{$this->title_field}]);
				$command = 'detail';



				return view('crudbooster::default.form', compact('row', 'page_menu', 'page_title', 'command', 'id'));
			}

			public function getSetMaintenanceNotes($id){
				$notes = Request::input('notes');
				$update['notes'] = $notes;
				$update['notes_by'] = CRUDBooster::myId();
				$update['notes_added_at'] = date('Y-m-d H:i:s');
				DB::table('maintenance')->where('id',$id)->update($update);

				header('location:'.$_SERVER['HTTP_REFERER']);
				exit;
			}

			public function getSetMaintenanceStatus($id,$status){

				$update = ['status'=>$status];

				switch($status){
					case 'Prendre en Charge':
								$update['taken_charge_by'] = CRUDBooster::myId();
								$update['taken_charge_at'] = date('Y-m-d H:i:s');
						break;
					case 'Terminé et Vérifié':
								$update['verified_by'] = CRUDBooster::myId();
								$update['fixed_at'] = date('Y-m-d H:i:s');
						break;
					case 'Terminé':
								$update['fixed_at'] = date('Y-m-d H:i:s');
					break;
				}

				DB::table('maintenance')->where('id',$id)->update($update);

				header('location:'.$_SERVER['HTTP_REFERER']);
				exit;
			}

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
					$cms_users_id = CRUDBooster::myId();
					$user = DB::table('cms_users')->where('id',$cms_users_id)->first();
					$ut = $user->id_cms_privileges;
					if($ut==3){
						$query->whereRaw("(status = 'Neuf' OR (status = 'Prendre en Charge' AND taken_charge_by = {$cms_users_id}) OR status = 'En attente de pièces de rechange')");
					}



	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here
					$b_type_users = DB::table('cms_users')->where('id_cms_privileges',3)->orWhere('id_cms_privileges',4)->get();
					if(!$b_type_users->isEmpty()){
						$data = [
							'priority'=>$postdata['priority'],
							'title'=>$postdata['title'],
							'where'=>$postdata['where']
						];
						foreach($b_type_users as $user){
							CRUDBooster::sendEmail(['to'=>$user->email,'data'=>$data,'template'=>'maintainers_new_maintenance_notification_fr']);
						}
					}
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */

			public function getSendReport(){
				$yesterday = date('Y-m-d',strtotime('-1days'));

				$yesterday_opened_maintenances = DB::table('maintenance')->whereRaw("DATE(created_at) = '{$yesterday}'")->get();
				$total_opened_maintenances = DB::table('maintenance')->whereRaw("status = 'Neuf' OR status = 'Prendre en Charge' OR status = 'En attente de pièces de rechange' OR status = ''")->get();
				$waiting_for_spare_parts_maintenances = DB::table('maintenance')->whereRaw("status = 'En attente de pièces de rechange'")->get();

				$data = [
					'yesterday_opened_maintenances' => count($yesterday_opened_maintenances),
					'total_opened_maintenances' => count($total_opened_maintenances),
					'waiting_for_spare_parts_maintenances' => count($waiting_for_spare_parts_maintenances)
				];

				echo '<pre>';
					print_r($data);
				echo '</pre>';

				CRUDBooster::sendEmail(['to'=>'massimiliano@mgc-group.it','data'=>$data,'template'=>'maintenance_report']);
				CRUDBooster::sendEmail(['to'=>'giojkd@gmail.com','data'=>$data,'template'=>'maintenance_report']);
				CRUDBooster::sendEmail(['to'=>'absesillo@gmail.com','data'=>$data,'template'=>'maintenance_report']);

			}


	    public function hook_after_add($id) {
	        //Your code here


	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)


	}
